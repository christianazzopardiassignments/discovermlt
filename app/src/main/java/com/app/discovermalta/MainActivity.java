package com.app.discovermalta;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.security.AuthProvider;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;

    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private LoginButton btnFBLogIn;
    private CallbackManager callbackManager;

    private Button btnLogIn;
    private EditText email;
    private EditText password;

    Intent registerIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();

        btnFBLogIn = findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();

        firebaseUser = firebaseAuth.getCurrentUser();

        FacebookSdk.sdkInitialize(getApplicationContext());

        btnFBLogIn.setReadPermissions(Arrays.asList("email"));

        password = findViewById(R.id.input_password_login);
        email = findViewById(R.id.input_email_login);
        btnLogIn = findViewById(R.id.LoginButton);

        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInAcc();
            }
        });

        btnFBLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInAccFB();
            }
        });

        if(firebaseUser!= null){
            startActivity(new Intent(MainActivity.this, Dashboard.class));
            Toast.makeText(getApplicationContext(), "Logged In : " + firebaseUser.getEmail(), Toast.LENGTH_LONG).show();
        }
    }

    //Code Runs at Start after creation of Class
    @Override
    protected void onStart() {
        super.onStart();
    }


    //On activity the Result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Button on Click Event
    public void signInAccFB(){
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Facebook Login Cancelled!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Authentication Error Occured!", Toast.LENGTH_LONG).show();
            }
        });
    }

    //Handles Facebook Token
    private void handleFacebookToken(AccessToken accessToken) {
        AuthCredential facebookCredential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(facebookCredential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            firebaseUser = firebaseAuth.getCurrentUser();
                            startActivity(new Intent(MainActivity.this, Dashboard.class));///TJISOSOFOAIF
                            Toast.makeText(getApplicationContext(), "Logged In : " + firebaseUser.getEmail(), Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Could not Login using Facebook", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    //To Sign in Account using Email & Password
    public void signInAcc(){
        String loginEmail = email.getText().toString().trim();
        String loginPassword = password.getText().toString().trim();

        if(TextUtils.isEmpty(loginEmail) || TextUtils.isEmpty(loginPassword)){
            Toast.makeText(MainActivity.this, "Fields cannot be Empty!", Toast.LENGTH_LONG).show();
        }
        else{
            firebaseAuth.signInWithEmailAndPassword(loginEmail, loginPassword)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        firebaseUser = firebaseAuth.getCurrentUser();
                        startActivity(new Intent(MainActivity.this, Dashboard.class));///TJISOSOFOAIF
                        Toast.makeText(getApplicationContext(), "Logged In : " + firebaseUser.getEmail(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(MainActivity.this, "Problem while signing in!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }


    //Display Register Account Activity
    public void displayRegisterMenu(View view){
        if (registerIntent == null){
            registerIntent = new Intent(this, RegisterAccountActivity.class);
        }
        startActivity(registerIntent);
    }

    //Opens DISCOVER Malta Website
    public void openWebsiteLink(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://discovermlt.000webhostapp.com/"));
        startActivity(intent);
    }


}
