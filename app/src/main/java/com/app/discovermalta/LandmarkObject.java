package com.app.discovermalta;

public class LandmarkObject {
    String imageUrl;
    String imageName;
    String linkUrl;

    public LandmarkObject(){
    }

    public LandmarkObject(String imageUrl, String imageName, String linkUrl) {
        this.imageUrl = imageUrl;
        this.imageName = imageName;
        this.linkUrl = linkUrl;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public String getLinkUrl() {
        return linkUrl;
    }
}
