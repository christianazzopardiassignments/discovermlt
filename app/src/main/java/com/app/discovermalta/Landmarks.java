package com.app.discovermalta;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.goodiebag.pinview.Pinview;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Landmarks extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = "Landmarks";

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    private Intent mainActi, dashActi, pinViewActi, appInfoActi;

    private DrawerLayout mDrawerMain;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mLinkUrls = new ArrayList<>();

    private ArrayList<LandmarkObject> landmarkList = new ArrayList<>();

    private DatabaseReference landmarkDatabase;
    private FirebaseDatabase myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landmarks);
        Log.d(TAG, "on Create : Started");

        mDrawerMain = findViewById(R.id.drawer_layout_landmarks);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerMain, R.string.open, R.string.close);
        mDrawerMain.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view_land);
        navigationView.setNavigationItemSelectedListener(this);
        initImageBitmaps();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout_landmarks);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps : preparing bitmaps.");

        String id = "0";
        int list = 0;
        Map newLandmark;
        LandmarkObject addLandmark;
        String newImage;
        String newImage_name;
        String newImage_URL;

        newImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Azure_Window_2009.JPG/1200px-Azure_Window_2009.JPG";
        newImage_name = "Azure Window [Destroyed]";
        newImage_URL = "https://en.wikipedia.org/wiki/Azure_Window";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://crisalist.files.wordpress.com/2015/08/for-once-not-a-landscape-blue-grotto-malta-1664x1109oc.jpg";
        newImage_name = "Blue Grotto";
        newImage_URL = "https://en.wikipedia.org/wiki/Blue_Grotto_(Malta)";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://www.aquarium.com.mt/wp-content/uploads/2017/05/malta_dinglicliffs.jpg";
        newImage_name = "Dingli Cliffs";
        newImage_URL = "https://www.tripadvisor.com/Attraction_Review-g1438796-d2533843-Reviews-Dingli_Cliffs-Dingli_Island_of_Malta.html";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://fracademic.com/pictures/frwiki/71/GharDalam-int%C3%A9rieur_Grotte.jpg";
        newImage_name = "Għar Dalam";
        newImage_URL = "https://en.wikipedia.org/wiki/G%C4%A7ar_Dalam";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://s3.amazonaws.com/gs-geo-images/76abdadf-d43a-48a7-8312-43ee62ee9c32.jpg";
        newImage_name = "Il-Maqluba Sinkhome";
        newImage_URL = "https://en.wikipedia.org/wiki/Maqluba_(Malta)";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Malta_Gozo_Inland_Sea_BW_2011-10-08_11-14-26.JPG/1200px-Malta_Gozo_Inland_Sea_BW_2011-10-08_11-14-26.JPG";
        newImage_name = "Inland Sea";
        newImage_URL = "https://en.wikipedia.org/wiki/Inland_Sea,_Gozo";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://www.visitgozo.com/wp-content/uploads/2016/07/Ggantija-Temples.jpg";
        newImage_name = "Ggantija Temples";
        newImage_URL = "http://heritagemalta.org/museums-sites/ggantija-temples/";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Facade_Hagar_Qim.jpg/1280px-Facade_Hagar_Qim.jpg";
        newImage_name = "Ħaġar Qim";
        newImage_URL = "https://en.wikipedia.org/wiki/Azure_Window";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://www.wondermondo.com/wp-content/uploads/2017/10/MnajdraRoof.jpg";
        newImage_name = "Mnajdra";
        newImage_URL = "https://www.wondermondo.com/mnajdra/";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://farm5.staticflickr.com/4006/4295415235_db7239d98c_z.jpg";
        newImage_name = "Skorba Temples";
        newImage_URL = "https://en.wikipedia.org/wiki/Skorba_Temples";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://www.ancient-origins.net/sites/default/files/field/image/Tarxien-Temples.jpg";
        newImage_name = "Tarxien Temples";
        newImage_URL = "http://heritagemalta.org/museums-sites/tarxien-temples/";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "http://www.skibbereeneagle.ie/web/wp-content/uploads/2014/05/Xaghra-Stone-Circle.jpg";
        newImage_name = "Xagħra Stone Circle";
        newImage_URL = "https://www.visitgozo.com/where-to-go-in-gozo/archaeological-sites/xaghra-stone-circle/";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Misrah_Ghar_il-Kbir_5.jpg/1200px-Misrah_Ghar_il-Kbir_5.jpg";
        newImage_name = "Misraħ Għar il-Kbir";
        newImage_URL = "https://en.wikipedia.org/wiki/Misra%C4%A7_G%C4%A7ar_il-Kbir";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        newImage = "http://paintmalta.com/wp-content/uploads/2016/01/Birgu_or_Vittoriosa.jpg";
        newImage_name = "Birgu";
        newImage_URL = "https://en.wikipedia.org/wiki/Birgu";
        if(landmarkDatabase != null) {
            list++;
            id = ""+list;
        }else{
            id = ""+list;
        }
        newLandmark = new HashMap();
        newLandmark.put("imageUrl", newImage);
        newLandmark.put("imageName", newImage_name);
        newLandmark.put("imageLink", newImage_URL);
        addLandmark = new LandmarkObject(newImage, newImage_name, newImage_URL);
        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks").child(id);
        landmarkDatabase.setValue(addLandmark);

        landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Landmarks");
        landmarkDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Azure_Window_2009.JPG/1200px-Azure_Window_2009.JPG");
        mNames.add("Azure Window [Destroyed]");
        mLinkUrls.add("https://en.wikipedia.org/wiki/Azure_Window");

        mImageUrls.add("https://crisalist.files.wordpress.com/2015/08/for-once-not-a-landscape-blue-grotto-malta-1664x1109oc.jpg");
        mNames.add("Blue Grotto");
        mLinkUrls.add("https://en.wikipedia.org/wiki/Blue_Grotto_(Malta)");

        mImageUrls.add("https://www.aquarium.com.mt/wp-content/uploads/2017/05/malta_dinglicliffs.jpg");
        mNames.add("Dingli Cliffs");
        mLinkUrls.add("https://www.tripadvisor.com/Attraction_Review-g1438796-d2533843-Reviews-Dingli_Cliffs-Dingli_Island_of_Malta.html");

        mImageUrls.add("https://fracademic.com/pictures/frwiki/71/GharDalam-int%C3%A9rieur_Grotte.jpg");
        mNames.add("Għar Dalam");
        mLinkUrls.add("https://en.wikipedia.org/wiki/G%C4%A7ar_Dalam");

        mImageUrls.add("https://s3.amazonaws.com/gs-geo-images/76abdadf-d43a-48a7-8312-43ee62ee9c32.jpg");
        mNames.add("Il-Maqluba Sinkhole");
        mLinkUrls.add("https://en.wikipedia.org/wiki/Maqluba_(Malta)");

        mImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Malta_Gozo_Inland_Sea_BW_2011-10-08_11-14-26.JPG/1200px-Malta_Gozo_Inland_Sea_BW_2011-10-08_11-14-26.JPG");
        mNames.add("Inland Sea");
        mLinkUrls.add("https://en.wikipedia.org/wiki/Inland_Sea,_Gozo");

        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView : Started");

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(mNames, mImageUrls, mLinkUrls, this);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            case R.id.menuItemDash:
                startDashboardActivity();
                break;
            case R.id.menuItemSear:

                break;
            case R.id.menuItemLand:
                break;
            case R.id.menuItemFave:
                startFavouritesActivity();
                break;
            case R.id.menuItemInfo:
                startAppInfoActivity();
                break;
            case R.id.menuItemWeb:
                startWebsite();
                break;
            case R.id.menuItemLogo:
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                Toast.makeText(getApplicationContext(), "Successfully Signed Out!", Toast.LENGTH_LONG).show();
                startMainActivity();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout_landmarks);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startMainActivity(){
        if (mainActi == null){
            mainActi = new Intent(this, MainActivity.class);
        }
        startActivity(mainActi);
    }

    public void startWebsite(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://discovermlt.000webhostapp.com/"));
        startActivity(intent);
    }

    public void startAppInfoActivity(){
        if (appInfoActi == null){
            appInfoActi = new Intent(this, AppInfo.class);
        }
        startActivity(appInfoActi);
    }

    public void startFavouritesActivity(){
        if (pinViewActi == null){
            pinViewActi = new Intent(this, PinView.class);
        }
        startActivity(pinViewActi);
    }

    public void startDashboardActivity(){
        if (dashActi == null){
            dashActi = new Intent(this, Dashboard.class);
        }
        startActivity(dashActi);
    }
}
