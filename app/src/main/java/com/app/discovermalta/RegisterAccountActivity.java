package com.app.discovermalta;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

public class RegisterAccountActivity extends AppCompatActivity{

    private EditText password, confirmPassword, email, confirmEmail;
    private Button btnSignUp;
    private FirebaseAuth firebaseAuth;
    Intent loginIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_account);

        firebaseAuth = FirebaseAuth.getInstance();

        btnSignUp = findViewById(R.id.btn_register);
        password = findViewById(R.id.input_password);
        confirmPassword = findViewById(R.id.input_password_confirm);
        email = findViewById(R.id.input_email);
        confirmEmail = findViewById(R.id.input_email_confirm);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String saveEmail = email.getText().toString().trim();
                String saveConfirmEmail = confirmEmail.getText().toString().trim();
                String savePassword = password.getText().toString().trim();
                String saveConfirmPassword = confirmPassword.getText().toString().trim();

                //Checks if Password is Empty
                if(TextUtils.isEmpty(savePassword)){
                    Toast.makeText(getApplicationContext(), "Please enter password!",Toast.LENGTH_SHORT).show();
                    return;
                }

                //Checks if Email is Empty
                if(TextUtils.isEmpty(saveEmail)){
                    Toast.makeText(getApplicationContext(), "Please enter email!",Toast.LENGTH_SHORT).show();
                    return;
                }

                //Checks if Confirm Email is Empty
                if(TextUtils.isEmpty(saveConfirmEmail)){
                    Toast.makeText(getApplicationContext(), "Please enter email!",Toast.LENGTH_SHORT).show();
                    return;
                }

                //Checks if password is longer than 6 characters
                if(savePassword.length() < 6){
                    Toast.makeText(getApplicationContext(), "Password too short, must be 6 characters or longer!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //Check if Email and Confirm Email Match
                if(!saveEmail.equals(saveConfirmEmail)){
                    Toast.makeText(getApplicationContext(),"Email does not match!",Toast.LENGTH_SHORT).show();
                    return;
                }

                //Checks if Passwords Match
                if(!savePassword.equals(saveConfirmPassword)){
                    Toast.makeText(getApplicationContext(),"Passwords do not match!",Toast.LENGTH_SHORT).show();
                    return;
                }

                firebaseAuth.createUserWithEmailAndPassword(saveEmail,savePassword).addOnCompleteListener(RegisterAccountActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(RegisterAccountActivity.this, "Account Exists Already!",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(RegisterAccountActivity.this, "Account created with Email : " + saveEmail, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterAccountActivity.this, MainActivity.class));
                            finish();
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //Display Login Account Activity
    public void displayLoginMenu(View view){
        if(loginIntent == null) {
            loginIntent = new Intent(this, MainActivity.class);
        }
        startActivity(loginIntent);
    }
}

