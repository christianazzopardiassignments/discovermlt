package com.app.discovermalta;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.goodiebag.pinview.Pinview;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AppInfo extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    private Intent mainActi, dashActi, searActio, landActi, pinViewActi, appInfoActi;

    private DrawerLayout mDrawerMain;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);

        mDrawerMain = findViewById(R.id.drawer_layout_app_info);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerMain, R.string.open, R.string.close);

        mDrawerMain.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view_app_info);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout_app_info);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            case R.id.menuItemDash:
                startDashboardActivity();
                break;
            case R.id.menuItemSear:
                //To create Code
                break;
            case R.id.menuItemLand:
                startLandmarkActivity();
                break;
            case R.id.menuItemFave:
                startFavouritesActivity();
                break;
            case R.id.menuItemInfo:
                break;
            case R.id.menuItemWeb:
                startWebsite();
                break;
            case R.id.menuItemLogo:
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                Toast.makeText(getApplicationContext(), "Successfully Signed Out!", Toast.LENGTH_LONG).show();
                startMainActivity();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout_app_info);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startMainActivity(){
        if (mainActi == null){
            mainActi = new Intent(this, MainActivity.class);
        }
        startActivity(mainActi);
    }

    public void startWebsite(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://discovermlt.000webhostapp.com/"));
        startActivity(intent);
    }

    public void startDashboardActivity(){
        if (dashActi == null){
            dashActi = new Intent(this, Dashboard.class);
        }
        startActivity(dashActi);
    }

    public void startFavouritesActivity(){
        if (pinViewActi == null){
            pinViewActi = new Intent(this, PinView.class);
        }
        startActivity(pinViewActi);
    }

    public void startLandmarkActivity(){
        if (landActi == null){
            landActi = new Intent(this, Landmarks.class);
        }
        startActivity(landActi);
    }
}
