package com.app.discovermalta;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements PopupMenu.OnMenuItemClickListener {

    public static final String TAG = "RecyclerVIewAdapter";
    private int selectedIndex;

    private DatabaseReference landmarkDatabase;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    private ArrayList<String> mImageNames;
    private ArrayList<String> mImage;
    private ArrayList<String> mLink;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView image;
        TextView image_name;
        RelativeLayout parent_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parent_layout = itemView.findViewById(R.id.recycler);
            image = itemView.findViewById(R.id.recycler_image);
            image_name = itemView.findViewById(R.id.recycler_image_name);
        }
    }

    public RecyclerViewAdapter(ArrayList<String> mImageNames, ArrayList<String> mImage, ArrayList<String> mLink, Context mContext) {
        this.mImageNames = mImageNames;
        this.mImage = mImage;
        this.mLink = mLink;
        this.mContext = mContext;

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,final int i) {
        Log.d(TAG, "onBindViewHolder : called");
        Glide.with(mContext).asBitmap().load(mImage.get(i)).into(viewHolder.image);
        viewHolder.image_name.setText(mImageNames.get(i));
        viewHolder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Clicked on : " + mImageNames.get(i));
                selectedIndex = i;
                showPopup(v);
            }
        });
    }

    public void showPopup(View v){
        PopupMenu popupMenu = new PopupMenu(mContext, v);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.popupmenu);
        popupMenu.show();
    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.land_favourite:
                String id;
                String user_ID = firebaseUser.getUid();
                if(landmarkDatabase != null) {
                    id = landmarkDatabase.push().getKey();
                }else{
                    id = "first_key";
                }
                landmarkDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(user_ID).child("landmarks").child(id);

                String newImage = mImage.get(selectedIndex);
                String newImage_name = mImageNames.get(selectedIndex);
                String newImage_URL = mLink.get(selectedIndex);

                Map newLandmark = new HashMap();
                newLandmark.put("imageUrl", newImage);
                newLandmark.put("imageName", newImage_name);
                newLandmark.put("imageLink", newImage_URL);

                landmarkDatabase.setValue(newLandmark);

                Toast.makeText(mContext, "Successfully added to Favourites!", Toast.LENGTH_LONG).show();
                break;
            case R.id.land_open_link:
                String url = mLink.get(selectedIndex);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                mContext.startActivity(intent);
        }
        return false;
    }
}
