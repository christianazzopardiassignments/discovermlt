package com.app.discovermalta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinView extends AppCompatActivity {

    Pinview pinview;
    Intent faveActi, dashActi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_view);

        pinview = findViewById(R.id.pin_view_menu);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                String userPin = pinview.getValue();
                if(userPin.equals("2019")){
                    startFavouritesActivity();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Error! Wrong pin!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void startFavouritesActivity(){
        if (faveActi == null){
            faveActi = new Intent(this, Favourites.class);
        }
        startActivity(faveActi);
    }

    public void startDashboardActivity(){
        if (dashActi == null){
            dashActi = new Intent(this, Dashboard.class);
        }
        startActivity(dashActi);
    }

    public void backToDash(View view){
        startDashboardActivity();
    }
}
