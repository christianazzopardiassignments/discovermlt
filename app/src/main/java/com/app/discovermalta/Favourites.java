package com.app.discovermalta;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.goodiebag.pinview.Pinview;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class Favourites extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "Favourites";

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    private Intent mainActi, dashActi, appInfoActi, landActi;

    private DrawerLayout mDrawerMain;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mLink = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        Log.d(TAG, "on Create : Started");

        mDrawerMain = findViewById(R.id.drawer_layout_favourites);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerMain, R.string.open, R.string.close);
        mDrawerMain.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.nav_view_fave);
        navigationView.setNavigationItemSelectedListener(this);
        initImageBitmaps();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout_favourites);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps : preparing bitmaps.");

        mImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Malta_Gozo_Inland_Sea_BW_2011-10-08_11-14-26.JPG/1200px-Malta_Gozo_Inland_Sea_BW_2011-10-08_11-14-26.JPG");
        mNames.add("Inland Sea");

        mImageUrls.add("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Azure_Window_2009.JPG/1200px-Azure_Window_2009.JPG");
        mNames.add("Azure Window [Destroyed]");

        mImageUrls.add("https://crisalist.files.wordpress.com/2015/08/for-once-not-a-landscape-blue-grotto-malta-1664x1109oc.jpg");
        mNames.add("Blue Grotto");

        mImageUrls.add("https://www.aquarium.com.mt/wp-content/uploads/2017/05/malta_dinglicliffs.jpg");
        mNames.add("Dingli Cliffs");

        mImageUrls.add("https://fracademic.com/pictures/frwiki/71/GharDalam-int%C3%A9rieur_Grotte.jpg");
        mNames.add("Għar Dalam");

        mImageUrls.add("https://s3.amazonaws.com/gs-geo-images/76abdadf-d43a-48a7-8312-43ee62ee9c32.jpg");
        mNames.add("Il-Maqluba Sinkhole");

        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView : Started");

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(mNames, mImageUrls, mLink, this);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            case R.id.menuItemDash:
                startDashboardActivity();
                break;
            case R.id.menuItemSear:

                break;
            case R.id.menuItemLand:
                startLandmarkActivity();
                break;
            case R.id.menuItemFave:
                break;
            case R.id.menuItemInfo:
                startAppInfoActivity();
                break;
            case R.id.menuItemWeb:
                startWebsite();
                break;
            case R.id.menuItemLogo:
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                Toast.makeText(getApplicationContext(), "Successfully Signed Out!", Toast.LENGTH_LONG).show();
                startMainActivity();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout_favourites);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startMainActivity(){
        if (mainActi == null){
            mainActi = new Intent(this, MainActivity.class);
        }
        startActivity(mainActi);
    }

    public void startWebsite(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://discovermlt.000webhostapp.com/"));
        startActivity(intent);
    }

    public void startAppInfoActivity(){
        if (appInfoActi == null){
            appInfoActi = new Intent(this, AppInfo.class);
        }
        startActivity(appInfoActi);
    }

    public void startLandmarkActivity(){
        if (landActi == null){
            landActi = new Intent(this, Landmarks.class);
        }
        startActivity(landActi);
    }

    public void startDashboardActivity(){
        if (dashActi == null){
            dashActi = new Intent(this, Dashboard.class);
        }
        startActivity(dashActi);
    }
}
